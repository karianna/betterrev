package org.adoptopenjdk.betterrev.models;

import org.adoptopenjdk.betterrev.ArquillianTransactionalTest;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Basic Interest persistence tests.
 * 
 * TODO could insert data from "initial-data.yml"
 */
public class InterestPersistenceTest extends ArquillianTransactionalTest {
    
    private static final String TEST_INTEREST_PATH = "Test Path";
    private static final String TEST_INTEREST_PROJECT = "Test Project";

    // TODO Parameterise the ArquillianTransactionalTest class so we can simply 
    // pass in what data to setup and destroy in each case
    @Before
    @Override
    public void preparePersistenceTest() throws Exception {
        clearData();
        insertData();
        startTransaction();
    }

    private void clearData() throws Exception {
        userTransaction.begin();
        entityManager.joinTransaction();
        entityManager.createQuery("DELETE FROM Interest").executeUpdate();
        userTransaction.commit();
    }

    private void insertData() throws Exception {
        userTransaction.begin();
        entityManager.joinTransaction();
        userTransaction.commit();
        // clear the persistence context (first-level cache)
        entityManager.clear();
    }

    private Interest createUnsavedInterest() {
        return new Interest(TEST_INTEREST_PATH, TEST_INTEREST_PROJECT);
    }

    @Test
    public void interestHasNoIdWhenFirstCreated() throws Exception {
        final Interest testInterest = createUnsavedInterest();
        assertNull(testInterest.getId());
    }

    @Test
    public void interestHasIdOnSave() throws Exception {
        final Interest testInterest = createUnsavedInterest();
        entityManager.persist(testInterest);
        assertNotNull(testInterest.getId());
    }
    
    @Test
    public void interestHasPathOnSave() throws Exception {
        final Interest testInterest = createUnsavedInterest();
        entityManager.persist(testInterest);
        assertEquals(TEST_INTEREST_PATH, testInterest.getPath());
    }

    @Test
    public void interestHasProjectOnSave() throws Exception {
        final Interest testInterest = createUnsavedInterest();
        entityManager.persist(testInterest);
        assertEquals(TEST_INTEREST_PROJECT, testInterest.getProject());
    }

    @Test
    public void onlyOneInterestIsPersisted() {
        final Interest testInterest = createUnsavedInterest();
        entityManager.persist(testInterest);
        List<Interest> interestList = entityManager.createNamedQuery("Interest.findAll", Interest.class).getResultList();
        assertEquals(1, interestList.size());
    }

    @Test
    public void findById() {
        final Interest testInterest = createUnsavedInterest();
        entityManager.persist(testInterest);
        long id = testInterest.getId();
        final Interest foundInterest = entityManager.find(Interest.class, id);
        assertEquals(foundInterest.getId(), testInterest.getId());
    }

}
