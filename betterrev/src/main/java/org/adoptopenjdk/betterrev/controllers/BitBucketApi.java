package org.adoptopenjdk.betterrev.controllers;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

public class BitBucketApi extends DefaultApi10a {

    public static final String USER_PROFILE = "https://bitbucket.org/api/1.0/user";

    @Override
    public String getRequestTokenEndpoint() {
        return "https://bitbucket.org/!api/1.0/oauth/request_token";
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://bitbucket.org/!api/1.0/oauth/access_token";
    }

    @Override
    public String getAuthorizationUrl(Token token) {
        return "https://bitbucket.org/!api/1.0/oauth/authenticate?oauth_token=" + token.getToken();
    }
}
