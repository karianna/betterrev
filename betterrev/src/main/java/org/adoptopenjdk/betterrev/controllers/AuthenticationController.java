package org.adoptopenjdk.betterrev.controllers;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@RequestScoped
@Path("authentication")
public class AuthenticationController {

    // TODO - make these configurable and change these to the "official" BetterRev Bitbucket key/secret
    private static final String KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyb9brjgOhXR8Rm85nOOsAvwwHK0SUibb1R41VXooaPCjiVt1XRvVTeuoq7U1WNS2gEnufUlKe7PxGFEYa0w0aEegz3px11WV84PIDTR0oeU98MFj8Q7lX4CFbArI2MLI1h8ymfQT1Kg/nBj0UhTa8/+6qPPGk51WNC7VUZZIq/YO8cVeBODLeNohAsItfDEqEs4KlPcbm9k35oNazpPPOXcaSp1s3xd6K7JKljs4T+FpqnZ2hmWcYAQPT7d756xIWJhShs8py4RRn8if7GumprB1IKdch4xJQ4TRxO4flQEfz+lQC6w9zVs4PeWp7iceckUTLcMdq0qaRPf6tjszx martijnverburg@gmail.com";
    private static final String SECRET = "Mystra01";

    @Context
    private UriInfo uriInfo;

    public ServiceBuilder setupOAuthService() {
        return new ServiceBuilder()
                .provider(BitBucketApi.class)
                .apiKey(KEY)
                .apiSecret(SECRET);
    }

    @GET
    @Path("signin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response signIn() throws URISyntaxException {
        final URI callbackUri = resolveCallbackUri();
        final OAuthService oAuthService = setupOAuthService().callback(callbackUri.toString()).build();
        final Token requestToken = oAuthService.getRequestToken();
        final String authenticationUrl = oAuthService.getAuthorizationUrl(requestToken);
        final Map<String, String> response = new HashMap<String, String>() {{
            put("authenticationUrl", authenticationUrl);
            put("oauthSecret", requestToken.getSecret());
        }};
        return Response.ok(response).build();
    }

    private URI resolveCallbackUri() {
        final String requestPath = uriInfo.getRequestUri().getPath();
        final int applicationPathIndex = requestPath.indexOf("betterrev/");
        final String baseRequestPath = requestPath.substring(0, applicationPathIndex);
        return uriInfo.getRequestUriBuilder().replacePath(baseRequestPath + "authenticate.html").build();
    }

    @GET
    @Path("authenticate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(
            @QueryParam("oauth_token") String token,
            @QueryParam("oauth_verifier") String verifier,
            @QueryParam("oauth_secret_token") String secretToken) {
        final OAuthService oAuthService = setupOAuthService().build();
        final Token accessToken = oAuthService.getAccessToken(new Token(token, secretToken), new Verifier(verifier));
        final Map<String, String> response = new HashMap<String, String>() {{
            put("oauthAccessToken", accessToken.getToken());
            put("oauthSecretToken", accessToken.getSecret());
        }};
        return Response.ok(response).build();
    }

    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response user(@QueryParam("oauth_secret_token") String secret,
                         @QueryParam("oauth_access_token") String accessTokenString) {
        final OAuthRequest userRequest = new OAuthRequest(Verb.GET, BitBucketApi.USER_PROFILE);
        final OAuthService oAuthService = setupOAuthService().build();
        final Token accessToken = new Token(accessTokenString, secret);
        oAuthService.signRequest(accessToken, userRequest);
        final org.scribe.model.Response userResponse = userRequest.send();
        if (!userResponse.isSuccessful()) {
            return Response.status(UNAUTHORIZED).build();
        }
        return Response.ok(userResponse.getBody()).build();
    }
}
