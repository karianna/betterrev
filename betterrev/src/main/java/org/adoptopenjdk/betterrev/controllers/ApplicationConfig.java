package org.adoptopenjdk.betterrev.controllers;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * This is the class with the magic ApplicationPath annotation 
 * which is what a Java EE 7 application needs in order to know it's hosting 
 * Restful Web services!
 * 
 * Do NOT remove unless you are a Java EE 7 expert and have decided that the 
 * development team got this wrong.
 */
@ApplicationPath("betterrev")
public class ApplicationConfig extends Application {
    // Deliberately do nothing
}
