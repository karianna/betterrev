package org.adoptopenjdk.betterrev.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Date;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

import org.adoptopenjdk.betterrev.BetterrevConfiguration;
import org.adoptopenjdk.betterrev.utils.FetchDiffFilesString;
import org.adoptopenjdk.betterrev.utils.FilenameExtractor;

/**
 * Contribution entity that is in essence a BetterRev enhanced version of a DVCS
 * pullrequest.<br>
 * <br>
 * For information here that corresponds to bitbucket information the bitbucket
 * pullrequest is the canonical source. e.g. createdOn is the creation date of
 * the pull request, not this object.
 */
@Entity
@Table(name = "Contribution")
@NamedQueries({
    @NamedQuery(name = "Contribution.findAllContributions",
            query = "SELECT contrib FROM Contribution contrib"),
    @NamedQuery(name = "Contribution.findContributionById",
            query = "SELECT contrib FROM Contribution contrib WHERE contrib.id = :contributionId")
})
public class Contribution implements Serializable {

    // TODO Check this owner field
    private static String owner = BetterrevConfiguration.INST.owner();

    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    @NotNull
    private String repositoryId;

    @NotNull
    private String pullRequestId;

    @NotNull
    private String name;

    private String description;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ContributionEvent> contributionEvents;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ContributionState contributionState;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Tag> tags;

    @ManyToOne
    private User requester;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Mentor> mentors = new HashSet<>();

    @NotNull
    private LocalDateTime createdOn;

    @NotNull
    private LocalDateTime updatedOn;

    // TODO See if we can do this in the JSON conversion layer instead
    @NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdOnForDisplay;

    @NotNull
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updatedOnForDisplay;

    @NotNull
    private String branchName;

    public Contribution() {
        this.tags = new HashSet<>();
        this.contributionEvents = new ArrayList<>();
    }

    public Contribution(String repositoryId, String pullRequestId, String name, String description, User requester,
            LocalDateTime createdOn, LocalDateTime updatedOn, String branchName) {
        this.tags = new HashSet<>();
        this.contributionEvents = new ArrayList<>();
        this.repositoryId = repositoryId;
        this.pullRequestId = pullRequestId;
        this.name = name;
        this.description = description;
        this.requester = requester;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.createdOnForDisplay = Date.from(createdOn.toInstant(ZoneOffset.UTC));
        this.updatedOnForDisplay = Date.from(updatedOn.toInstant(ZoneOffset.UTC));
        this.branchName = branchName;
        this.contributionState = ContributionState.NULL;

        String diffFilesString = FetchDiffFilesString.from(pullRequestUrlForOwner());
        this.mentors = evaluateMentorsFrom(diffFilesString);
    }

    public boolean wasUpdatedBefore(LocalDateTime updated) {
        return this.getUpdatedOn().isBefore(updated);
    }

    public boolean hasContributionEventWith(@NotNull ContributionEventType contributionEventType) {
        final List<ContributionEvent> contributionEvents = getContributionEvents();
        return contributionEvents.stream().anyMatch(
                (event) -> contributionEventType.equals(event.getContributionEventType()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(pullRequestId, repositoryId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Contribution other = (Contribution) obj;
        return Objects.equals(pullRequestId, other.pullRequestId)
                && Objects.equals(repositoryId, other.repositoryId);

    }

    // https://bitbucket.org/api/2.0/repositories/AdoptOpenJDK/better-test-repo/pullrequests/1/diff
    public final String pullRequestUrlForOwner() {
        return String.format("https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff",
                owner, repositoryId, pullRequestId);
    }

    // https://bitbucket.org/api/2.0/repositories/richardwarburton/better-test-repo/pullrequests/1/diff
    public String pullRequestUrl() {
        return String.format("https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff",
                getRequester().getBitbucketUserName(), repositoryId, pullRequestId);
    }

    public final Set<Mentor> evaluateMentorsFrom(String inDiffFileString) {
        Set<String> filesChanged = FilenameExtractor.extractFilenamesFromPullRequestDiffText(inDiffFileString);
        Set<Mentor> results = new HashSet<>();

        results.addAll(Mentor.findRelevantMentors(repositoryId, filesChanged));
        return results;
    }

    public String mentorEmailsUrl() {
        StringBuilder mentorEmailsUrlBuilder = new StringBuilder().append("mailto:");

        int numberOfEmailsAdded = 0;
        int numberOfMentors = getMentors().size();

        for (Mentor mentor : getMentors()) {
            mentorEmailsUrlBuilder.append(mentor.getEmail());

            numberOfEmailsAdded++;
            if (numberOfEmailsAdded < numberOfMentors) {
                mentorEmailsUrlBuilder.append(",");
            }
        }

        return mentorEmailsUrlBuilder.toString();
    }

    public String requestersRepositoryUrl() {
        return String.format("ssh://hg@bitbucket.org/%s/%s", getRequester().getBitbucketUserName(), repositoryId);
    }

    /**
     * e.g. 'corba', 'hotspot', '.'
     *
     * @return OpenJDK Mercurial repository name
     */
    public String openJdkRepoName() {
        String[] split = repositoryId.split("-");

        // Subrepo case
        if (split.length == 2) {
            return split[1];
        }

        // Top level repo case
        return ".";
    }

    public static Contribution findByBitbucketIds(String repositoryId, String requestId) {
        // return find.where().eq("repositoryId", repositoryId).eq("pullRequestId", requestId).findUnique();
        // TODO port this to Java EE 7
        return null;
    }

    public File webrevLocation() {
        String relativePath = String.format("public/webrevs/webrev-%s-%s/", repositoryId, pullRequestId);
        return new File(relativePath).getAbsoluteFile();
    }

    public boolean isDefaultBranch() {
        return "default".equals(branchName);
    }

    // TODO null check may be unnecessary
    public Collection<ContributionEvent> getContributionEventWithType(ContributionEventType contributionEventType) {
        List<ContributionEvent> eventsOfAParticularType = this.contributionEvents.stream()
                .filter(
                    (event) -> (event.getContributionEventType() != null && 
                                event.getContributionEventType().equals(contributionEventType))
                )
                .collect(Collectors.toList());

        return eventsOfAParticularType ;
    }

    public Long getId() {
        return id;
    }

    // TODO remove later
    public void setKey(Long key) {
        id = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }

    public ContributionState getContributionState() {
        return contributionState;
    }

    public void setContributionState(ContributionState contributionState) {
        this.contributionState = contributionState;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public List<ContributionEvent> getContributionEvents() {
        return contributionEvents;
    }

    public void setContributionEvents(List<ContributionEvent> contributionEvents) {
        this.contributionEvents = contributionEvents;
    }

    public Set<Mentor> getMentors() {
        return mentors;
    }

    public void setMentors(Set<Mentor> mentors) {
        this.mentors = mentors;
    }
}
