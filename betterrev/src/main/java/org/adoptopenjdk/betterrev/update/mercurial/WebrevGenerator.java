package org.adoptopenjdk.betterrev.update.mercurial;

import java.io.IOException;

import org.adoptopenjdk.betterrev.models.Contribution;
import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.ContributionEventType;
import org.adoptopenjdk.betterrev.update.BetterrevActor;
import org.adoptopenjdk.betterrev.utils.Processes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.adoptopenjdk.betterrev.models.ContributionEventType.CONTRIBUTION_GENERATED;
import static org.adoptopenjdk.betterrev.models.ContributionEventType.CONTRIBUTION_MODIFIED;
import static org.adoptopenjdk.betterrev.models.ContributionEventType.WEBREV_GENERATED;

/**
 * Generates webrevs based upon a Contribution diffed against the current openjdk repository
 */
public class WebrevGenerator extends BetterrevActor {

    private final static Logger LOGGER = LoggerFactory.getLogger(WebrevGenerator.class);
    
    private static final String WEBREV_SUFFIX = "scripts/generate_webrev.sh";

    private final String adoptDirectory;

    public WebrevGenerator() {
        this.adoptDirectory = "adopt";
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof ContributionEvent)) {
            unhandled(message);
            return;
        }

        ContributionEvent event = (ContributionEvent) message;
        ContributionEventType type = event.getContributionEventType();
        if (type == CONTRIBUTION_GENERATED || type == CONTRIBUTION_MODIFIED) {
            generateWebrev(event, type);
        }
    }

    private void generateWebrev(ContributionEvent event, ContributionEventType type) throws IOException {
        LOGGER.debug("ContributionEvent of type " + type + " received.");

        int exitCode = runWebrevGeneratorScript(event);
        LOGGER.info("Generated the webrev for " + event);

        if (exitCode != 0) {
            // TODO: Improve error handling around this process
            LOGGER.error("generateWebrev fails with exit code [" + exitCode + "]");
            throw new IOException("generateWebrev fails with exit code [" + exitCode + "]");
        }

        ContributionEvent webRevGenerated = new ContributionEvent(WEBREV_GENERATED);
        eventStream().publish(webRevGenerated);
    }

    private int runWebrevGeneratorScript(ContributionEvent event) throws IOException {
        Contribution contribution = event.getContribution();
        String repository = contribution.openJdkRepoName();
        String remote = contribution.requestersRepositoryUrl();
        String resultLocation = contribution.webrevLocation().getPath();

        int exitCode = Processes.runThroughShell(adoptDirectory, WEBREV_SUFFIX, repository, remote, resultLocation);
        return exitCode;
    }

}
