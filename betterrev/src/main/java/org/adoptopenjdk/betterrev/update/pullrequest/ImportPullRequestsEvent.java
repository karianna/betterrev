package org.adoptopenjdk.betterrev.update.pullrequest;

import org.adoptopenjdk.betterrev.events.BetterrevEvent;

import com.fasterxml.jackson.databind.JsonNode;

public final class ImportPullRequestsEvent extends BetterrevEvent {

    private final JsonNode jsonNode;
    private final String project;

    public ImportPullRequestsEvent(JsonNode jsonNode, String project) {
        this.jsonNode = jsonNode;
        this.project = project;
    }

    public JsonNode getJsonNode() {
        return jsonNode;
    }

    public String getProject() {
        return project;
    }
}
