package org.adoptopenjdk.betterrev.update.mercurial;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.adoptopenjdk.betterrev.utils.Processes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aragost.javahg.BaseRepository;
import com.aragost.javahg.Changeset;
import com.aragost.javahg.Repository;
import com.aragost.javahg.commands.flags.LogCommandFlags;

/**
 * Pulls in code from a Mercurial repository, keeping an internal hg repository up to date.
 */
public class MercurialImporter {

    private final static Logger LOGGER = LoggerFactory.getLogger(MercurialImporter.class);
    
    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("> EEE MMM dd HH:mm:ss YYYY Z");

    private static final String IMPORT_SCRIPT = "get_source.sh";

    private static final List<String> SUB_REPOS = Arrays.asList("corba", "hotspot", "jaxp", "jaxws", "jdk", "langtools", "nashorn");

    // TODO See Issue #14 This value is never used
    private final String adoptDirectory;
    
    private final String topLevelRepository;

    public MercurialImporter(String adoptDirectory, String topLevelRepoName) {
        this.adoptDirectory = adoptDirectory;
        topLevelRepository = adoptDirectory + "/" + topLevelRepoName;
    }

    public int doImport() {
        try {
            LOGGER.debug("About to run [" + topLevelRepository + "/" + IMPORT_SCRIPT + "]");
            return Processes.runThroughShell(topLevelRepository, IMPORT_SCRIPT);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return Integer.MIN_VALUE;
        }
    }

    public Map<String, List<Changeset>> listChangesets(LocalDateTime lastImport) {
        Map<String, List<Changeset>> changes = new HashMap<>();

        for (String repositoryName : SUB_REPOS) {
            File repositoryLocation = new File(topLevelRepository, repositoryName);
            BaseRepository repository = Repository.open(repositoryLocation);
            try {
                List<Changeset> changesets = LogCommandFlags.on(repository)
                        .date(formatDate(lastImport))
                        .execute();

                if (!changesets.isEmpty()) {
                    changes.put(repositoryName, changesets);
                }
            } finally {
                repository.close();
            }
        }

        return changes;
    }

    public static String formatDate(LocalDateTime dateTime) {
        return dateTime.format(FORMAT);
    }

}
