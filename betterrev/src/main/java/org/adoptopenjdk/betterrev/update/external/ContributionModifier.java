package org.adoptopenjdk.betterrev.update.external;

import org.adoptopenjdk.betterrev.models.Contribution;
import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.ContributionEventType;
import org.adoptopenjdk.betterrev.models.ContributionState;

/**
 * Service Class responsible for modifying a Contribution (and updating the lifecycle events) as specified by
 * actions occurring external to the DVCS polling.<br>
 * <br>
 * Developers Note: The following methods are effectively stubs which can be augmented with functionality
 * related to the underlying task.
 */
public final class ContributionModifier {

    private ContributionModifier() {
        //  Hide Utility Class Constructor - Utility classes should not have a public or default constructor.
    }

    public static Contribution notifyMentor(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, ContributionState.PENDING_PEER_REVIEW);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.MENTOR_NOTIFIED, linkToExternalInfo));
        contribution.setContributionState(ContributionState.PENDING_MENTOR_APPROVAL);
        
        // TODO JPA update
        //contribution.update();

        return contribution;
    }
    
    public static Contribution checkOcaStatus(Contribution contribution) {
        ensureExpectedStartState(contribution, ContributionState.PENDING_PEER_REVIEW);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.WAITING_FOR_OCA_SIGNATURE, contribution));

        return contribution;
    }

    public static Contribution terminateContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, ContributionState.OPEN);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.TERMINATED, linkToExternalInfo));
        contribution.setContributionState(ContributionState.CLOSED);
        
        // TODO JPA update
        //contribution.update();

        return contribution;
    }

    public static Contribution mentorRejectContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, ContributionState.PENDING_MENTOR_APPROVAL);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.REJECTED, linkToExternalInfo));
        contribution.setContributionState(ContributionState.CLOSED);

        // TODO JPA update
        //contribution.update();

        return contribution;
    }

    public static Contribution mentorApproveContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, ContributionState.PENDING_MENTOR_APPROVAL);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.APPROVED, linkToExternalInfo));
        contribution.setContributionState(ContributionState.ACCEPTED);

        // TODO JPA update
        //contribution.update();

        return contribution;
    }

    public static Contribution mergeContribution(String repositoryId, String requestId, String linkToExternalInfo) {
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        ensureExpectedStartState(contribution, ContributionState.ACCEPTED);

        contribution.getContributionEvents().add(
                new ContributionEvent(ContributionEventType.MERGED, linkToExternalInfo));
        contribution.setContributionState(ContributionState.COMMITTED);

        // TODO JPA update
        //contribution.update();

        return contribution;
    }

    private static void ensureExpectedStartState(Contribution contribution, ContributionState expectedState) {
        if (contribution.getContributionState() != expectedState) {
            throw new IllegalStateException("Cannot transition from current State of " + contribution.getContributionState());
        }
    }

}
