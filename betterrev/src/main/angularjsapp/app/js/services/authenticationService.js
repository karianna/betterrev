betterrevApp.factory('authenticationService', ['$window', '$http', 'environment', function ($window, $http, environment) {

    function isAuthenticated() {
        return $window.localStorage.hasOwnProperty('isAuthenticated');
    }

    var userDetails = {};

    if (isAuthenticated()) {
        $http({
            url: environment.baseURL + 'betterrev/authentication/user',
            method: 'GET',
            params: {
                oauth_access_token: $window.localStorage["oauthAccessToken"],
                oauth_secret_token: $window.localStorage["oauthSecretToken"]
            }
        }).success(function (userData) {
            angular.extend(userDetails, userData);
        }).error(function () {
            $window.localStorage.removeItem('oauthAccessToken');
            $window.localStorage.removeItem('oauthSecretToken');
            $window.localStorage.setItem('isAuthenticated', false);
            loadAuthenticationUrl();
        });
    } else {
        loadAuthenticationUrl();
    }

    var authenticationUrl;

    function loadAuthenticationUrl() {
        $http({
            url: environment.baseURL + 'betterrev/authentication/signin',
            method: 'GET'
        }).success(function (data) {
            $window.localStorage.setItem('oauthSecret', data['oauthSecret']);
            authenticationUrl = data['authenticationUrl'];
        });
    }

    return {
        authenticate: function () {
            $window.open(authenticationUrl, 'BitBucket SignIn', 'location=0,status=0,width=400,height=400');
        },
        isAuthenticated: isAuthenticated,
        getUserDetails: function () {
            return userDetails;
        }
    };
}]);