module.exports = function(config) {
    config.set({
        basePath : '../',
        frameworks : [ 'mocha', 'chai', 'chai-as-promised', 'sinon-chai' ],
        reporters : [ 'dots' ],
        browsers : [ 'PhantomJS' ],
        captureTimeout : 60000,
        logLevel: config.LOG_DEBUG,

        // web server port
        port : 9876,

        // cli runner port
        runnerPort : 9100,

        // app files
        files : [

        // 3rd Party Code
        '../../webapp/js/lib.js',

        // extra testing code
        //'../vendor/bower/angular-mocks/angular-mocks.js',

        // app-specific code
        '../../webapp/js/app.js',

        // test-specific code
        // '../node_modules/chai/chai.js',

        // tests
        'unit/**/*.spec.js'
        ]
    });
};
